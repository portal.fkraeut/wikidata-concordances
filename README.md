# About

Script to query Wikidata for concordances of place names in [Pelagios](http://www.pelagios.org). Uses Pelagios data as input, found here: https://github.com/pelagios/recogito2-places-geonames/releases

# How to

Start docker containers with
```
docker-compose up -d
```

Place jsonl dump in /input folder and check file name in scripts/findconcordance.py

Attach to container with
```
docker exec -it wikidata_concordance_scraper bash
```

Run script and output to csv with
```
python /scripts/findConcordance.py > /output/concordance.csv
```