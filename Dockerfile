FROM ubuntu:14.04
RUN apt-get update && apt-get install -y \
  software-properties-common \
  python-software-properties && \
  add-apt-repository ppa:fkrull/deadsnakes && \
  apt-get update && \
  apt-get install -y \
  wget \
  curl \
  unzip \
  python2.7 \
  python-pip \
  python-setuptools \
  python-dev \
  libxml2-dev \
  libxslt1-dev \
  lib32z1-dev
RUN pip install lxml rdflib jsonlines
RUN wget http://github.com/norcalrdf/pymantic/archive/master.zip && unzip master.zip
WORKDIR /pymantic-master
RUN python setup.py install
WORKDIR /
RUN mkdir scripts
COPY ./scripts/ /scripts
RUN mkdir input
RUN mkdir output
VOLUME /input
VOLUME /output
VOLUME /scripts
CMD ["tail", "-f", "/dev/null"]
