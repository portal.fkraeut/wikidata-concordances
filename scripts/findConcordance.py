import jsonlines
from pymantic import sparql

inputFile = '/input/geonames-20170404.jsonl'
endpoint = sparql.SPARQLServer('http://query.wikidata.org/sparql')

query = """
SELECT ?place ?label ?geonames_place ?freebase_place ?gnd_place ?viaf_place WHERE {
  BIND('%(geonames_place_id)s' as ?geonames_place_id)
  BIND(uri(concat("http://sws.geonames.org/", ?geonames_place_id)) AS ?geonames_place)
  ?place wdt:P1566 ?geonames_place_id ;
    rdfs:label ?label .
    FILTER(LANG(?label) = 'en')
 OPTIONAL {
    ?place wdt:P646 ?freebase_place
 }
 OPTIONAL {
  ?place wdt:P227 ?gnd_place
 }
 OPTIONAL {
  ?place wdt:P214 ?viaf_place
 }
} LIMIT 1
 """

maxExec=1000000
hasExec=0

headers=['place','geonames_place','freebase_place','gnd_place','viaf_place','label']

print(','.join(headers))

with jsonlines.open(inputFile) as reader :
    for obj in reader :
        geonames_id = obj['uri'][24:]
        response = endpoint.query( query % dict(geonames_place_id=geonames_id) )
        for row in response['results']['bindings']:
            output = []
            for header in headers:
                if header in row:
                    output.append(row[header]['value'])
                else :
                    output.append("")
            print(','.join(output).encode('utf-8'))
        hasExec+=1
        if hasExec>=maxExec :
            print("Maximum execution reached")
            break
